Rails.application.routes.draw do
  resources :carousel, only: [:index] do
    collection do
      get :next_blocks
    end
  end

  match '*path', to: ->(_env) { [404, { Error: 'Endpoint not implemented' }, []] }, via: :all

  root to: 'carousel#index'
end
