# Installation

1. `bundle install`

2. `gem install foreman`

3. `foreman s -e .env` OR run commands in `Procfile` individually.
