class CarouselController < ApplicationController
  def index; end

  def next_blocks
    render json: { blocks: load_blocks }, status: :ok
  end

  private

  def load_blocks
    cur_idx = blocks_params[:index].to_i
    blocks  = []

    (1..4).each do |n|
      blocks << {
        title: "Block #{cur_idx + n}",
        images: Carousel::IMAGES_URLS.sample([2, 3, 4, 5].sample)
      }
    end

    blocks
  end

  def blocks_params
    params.require(:block).permit(:index)
  end
end
