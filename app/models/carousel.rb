class Carousel
  IMAGES_URLS = [
    'https://www.gstatic.com/webp/gallery/1.sm.jpg',
    'https://www.gstatic.com/webp/gallery/2.sm.jpg',
    'https://www.gstatic.com/webp/gallery/3.sm.jpg',
    'https://www.gstatic.com/webp/gallery/4.sm.jpg',
    'https://www.gstatic.com/webp/gallery/5.sm.jpg',
  ]
end
