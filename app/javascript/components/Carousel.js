import React from "react"
import PropTypes from "prop-types"

const Arrow = ({ onClick, direction, disabled }) =>
  <div
    className={`absolute pointer arrow-${direction}`}
    style={{
      top: 0,
      right: direction === 'right' && '0px',
      zIndex: 1,
      width: 40,
      height: '100%',
      cursor: disabled && 'not-allowed',
    }}
    onClick={onClick}
  >
    <div
      className="absolute"
      style={{ top: 50.5, left: direction === 'right' ? '5px' : '24px', color: 'white' }}
    >
      <i className={`fa fa-chevron-${direction}`} />
    </div>
  </div>;

const NextArrow = (props) =>
  <Arrow direction="right" { ...props } />;

const PrevArrow = (props) =>
  <Arrow direction="left"  { ...props }/>;

const Block = ({ img_url, title }) =>
  <div className="relative block-fade left" style={{ height: '100%', width: '100%', border: 'solid 1px white' }}>
    <div
      className="bg-center bg-cover bg-no-repeat center"
      style={{
        backgroundImage: `url(${img_url})`,
        backgroundColor: 'red',
        height: '100%',
        color: 'white'
      }}
    >
      <strong>{title}</strong>
    </div>
  </div>;

class Carousel extends React.Component {
  state = {
    blockIdx: 0,
    noOfBlocks: 4,
    blocks: [],
    fetched: [],
  };

  componentWillMount = () => {
    $.ajax({
      url: '/carousel/next_blocks',
      data: {
        block: {
          index: this.state.blockIdx,
        },
      },
      success: (resp) => {
        this.setState({
          blockIdx: this.state.blockIdx + this.state.noOfBlocks,
          blocks: this.state.blocks.concat(resp.blocks),
          fetched: this.state.fetched.concat([this.state.blockIdx]),
        });
      },
    });
  }

  prevBlocks = () => {
    const {
      blockIdx,
      noOfBlocks,
    } = this.state;

    if (!blockIdx || blockIdx - noOfBlocks <= 0) {
      return;
    }

    this.setState({ blockIdx: blockIdx - noOfBlocks });
  }

  nextBlocks = () => {
    if (this.state.fetched.indexOf(this.state.blockIdx) >= 0) {
      this.setState({
        blockIdx: this.state.blockIdx + this.state.noOfBlocks,
      });

      return;
    }

    $.ajax({
      url: '/carousel/next_blocks',
      data: {
        block: {
          index: this.state.blockIdx,
        },
      },
      success: (resp) => {
        this.setState({
          blockIdx: this.state.blockIdx + this.state.noOfBlocks,
          blocks: this.state.blocks.concat(resp.blocks),
          fetched: this.state.fetched.concat([this.state.blockIdx]),
        });
      },
    });
  }

  render () {
    const {
      blockIdx,
      noOfBlocks,
      blocks,
    } = this.state;

    return (
      <div
        className="my1 col col-12 flex"
        style={{ height: '120px', backgroundColor: 'black', marginTop: '25%' }}
      >
        <div className="slider initialized">
          <PrevArrow
            disabled={blockIdx - noOfBlocks <= 0}
            onClick={this.prevBlocks}
          />
          <div className="list flex justify-center">
            <div
              className="track flex flex-auto justify-center"
              style={{ width: '100%', opacity: 1, transform: 'translate3d(0px, 0px, 0px)' }}
            >
              {blocks.map((block, idx) =>
                (blockIdx - noOfBlocks <= idx && idx < blockIdx) ?
                    <Block
                      key={idx}
                      img_url={block.images[Math.floor(Math.random() * block.images.length)]}
                      title={block.title}
                    />
                  : ''
                )
              }
            </div>
          </div>
          <NextArrow
            onClick={this.nextBlocks}
          />
        </div>
      </div>
    );
  }
}

export default Carousel;
